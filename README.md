- `docker swarm init`
- `docker pull registry.gitlab.com/sadesyllas/riak:<version>`
  - (optional) if this command is not used the next command will do this
but no progress will be displayed as the image is downloaded for the first time
- `docker stack deploy -c riak.yml riak`
- check http://localhost:8098/admin
